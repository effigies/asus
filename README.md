# ASUS Router Scripts

These scripts are used to push [cron][crontab] jobs to an ASUS router.
Specifically, the cron job being pushed updates the DNS record for
the router using the [name.com API][name-api].

This should be adaptable to other jobs and routers.

## How to set up

Create an `env.sh` file like the following:

```bash
# Router info
ROUTER=user@host                                # Login information for the router
DEV=ppp0                                        # Name of the WAN interface

# Name.com info
USER=username                                   # name.com login
TOKEN=0000000000000000000000000000000000000000  # Generate token at https://www.name.com/account/settings/api
DOMAIN=                                         # Domain name managed by name.com
HOST=                                           # Host under $DOMAIN (FQDN=$HOST.$DOMAIN)
RECORD=000000000                                # Retrieve all records with `curl -u $USER:$TOKEN $API/domains/$DOMAIN/records`
                                                # Find the object matching "host" and use the "id" field
TTL=300                                         # TTL in seconds
```

These variables are used by the `push` script to push jobs on the router.

## How it works

The ASUS router runs `crond`, which will run the crontabs in
`/var/spool/cron/crontabs`, but this is a volatile directory that is cleared at
every reboot.
A persistent `/jffs` mount is the only place we can save scripts,
except for name-value pairs in `nvram`.

### Init script

The [jffs/custom-scripts/init](jffs/custom-scripts/init) script writes a crontab for
the admin user, restarts the [cron daemon][crond], and installs itself to be run
automatically on USB mount.
The cron job calls the `update-name-dns` script hourly.

The init script also posts an update to the syslog that is viewable from the router
web interface, for debugging purposes.

### DNS update script

The [jffs/custom-scripts/update-name-dns](jffs/custom-scripts/update-name-dns) script
reads the IP address from the device `$DEV` (defined above in `env.sh`) and updates
the A-record `$RECORD` to direct `$HOST.$DOMAIN` to that address.

The variables from `env.sh` that are needed to accomplish this are placed in a
`jffs/custom-scripts/vars` file during the push

### Push script

The [push](push) script saves the relevant variables to `jffs/custom-scripts/vars`.
The `jffs/custom-scripts` directory is copied into a `/jffs/custom-scripts`
directory on the router, and `/jffs/custom-scripts/init` is called to install
the cron job.

If you plug in a USB drive to your router, it should re-`init` itself on boot.
If you can't find one or it fails for some reason, `push` again and the `crontab`
will be reinstalled.

### Fetch script

In case you modify your scripts on the router, the [fetch](fetch) script will
retrieve them.

## Troubleshooting

If you are editing this and running into issues, be aware that the shell and many
utilities on the router are actually [busybox][], which will not have all the
features of bash/coreutils. The SSH daemon also seems limited, hence the use of
`tar` instead of `scp`.

[crontab]: https://www.man7.org/linux/man-pages/man5/crontab.5.html
[crond]: https://www.man7.org/linux/man-pages/man8/crond.8.html
[name-api]: https://www.name.com/api-docs
[busybox]: https://www.busybox.net/
