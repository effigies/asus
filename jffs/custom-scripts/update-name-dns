#!/bin/bash
#
# Name.com DNS update script
#
# 2022 Chris Markiewicz
#
# Released into the public domain
#
# This is a script I run on my router. Mostly intended to prevent having to
# figure it out again when I get a new router.
# The API docs can be found here: https://www.name.com/api-docs
set -e

# A vars file must be in the same directory
source $( dirname $0 )/vars

CMD=$( basename $0 )

API=https://api.name.com/v4

URL=$API/domains/$DOMAIN/records/$RECORD
IP=$( ip address show dev $DEV | awk '/inet / { sub("/24", ""); print $2 }' )

curl -u $USER:$TOKEN $URL -X PUT -H 'Content-Type: application/json' \
    --data '{"host": "'$HOST'", "type": "A", "answer": "'$IP'", "ttl": '$TTL'}' \
  | logger -s -t $CMD
